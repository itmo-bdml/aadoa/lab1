# lab work 1
## Experimental time complexity analysis


[Report (google doc)](https://docs.google.com/document/d/1rm2gpAXmzCslCgJr7jsMamb13_1_bV9Rwi92uX66Ugw)


Performed by:
* Elya Avdeeva
* Gleb Mikloshevich



### Goal
Experimental study of the time complexity of different algorithms
### Problems and methods
For each n from 1 to 2000, measure the average computer execution time (using
timestamps) of programs implementing the algorithms and functions below for five
runs. Plot the data obtained showing the average execution time as a function of n.
Conduct the theoretical analysis of the time complexity of the algorithms in question
and compare the empirical and theoretical time complexities.
---
I. Generate an n-dimensional random vector $`v=[v_1, v_2, ..., v_n]`$. with non-negative
elements. For v, implement the following calculations and algorithms:
1) $`f(v)=const`$ (constant function);

<img src="./lab_work/One/plots/1.png" height="250">

2) $`f(V)=const`$ (the sum of elements);

<img src="./lab_work/One/plots/2.png" height="250">

3) $`f(V)=const`$ (the product of elements);

<img src="./lab_work/One/plots/3.png" height="250">

4) supposing that the elements of v are the coefficients of a polynomial P of
degree n − 1, calculate the value P(1.5)


a) by a direct calculation of P(x)

<img src="./lab_work/One/plots/4_1.png" height="250">

b) by Horner’s method

<img src="./lab_work/One/plots/4_2.png" height="250">

5) Bubble Sort of the elements of v;

<img src="./lab_work/One/plots/5.png" height="250">

6) Quick Sort of the elements of v;

<img src="./lab_work/One/plots/6.png" height="250">

7) Timsort of the elements of v.

<img src="./lab_work/One/plots/7.png" height="250">

---
II. Generate random matrices A and B of size n × n with non-negative elements.
Find the usual matrix product for A and B

<img src="./lab_work/Two/1.png" height="250">
