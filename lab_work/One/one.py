import random
from time import perf_counter

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import seaborn as sns

# Constants
num_runs = 5
n_values = list(range(1, 2001))


# Function to calculate f(v) = const
def const_fun(v):
    return 42


# Function to calculate sum of elements from 1 to n
def sum_elems(v):
    return sum(v[:len(v)])


# Function to measure execution time for constant_function
def measure_time(n, num_runs):
    total_time = 0
    for _ in range(num_runs):
        v = [random.randint(0, 100) for _ in range(n)]  # Generate a random vector
        start_time = perf_counter()
        const_fun(v)
        end_time = perf_counter()
        total_time += end_time - start_time
        return total_time / num_runs  # Average execution time


execution_times = []

# Measure execution time for each n
for n in n_values:
    avg_time = measure_time(n, num_runs)
    execution_times.append(avg_time)

# Theoretical analysis (constant time complexity)
theoretical_complexity = [0] * len(n_values)


# Plot the data
sns.set_theme()
sns.lineplot(x=n_values, y=execution_times, label="Empirical Time Complexity", )
sns.lineplot(x=n_values, y=theoretical_complexity, label="Theoretical Time Complexity (O(1))", linestyle='--')
plt.xlabel('n')
plt.ylabel('Average Execution Time (s)')
plt.title('Empirical vs. Theoretical Time Complexity of the constant function')
plt.legend()
plt.grid(True)
plt.savefig("1.png")
plt.show()
