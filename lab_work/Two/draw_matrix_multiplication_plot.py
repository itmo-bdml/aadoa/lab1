import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
matplotlib.use('TkAgg')

# Constants
num_runs = 5
n_values = list(range(1, 2001))

# Theoretical analysis (linear time complexity)
theoretical_complexity = [0.0000000007 * i**3 + 0.00000016 * i**2  for i in range(1, 2001)]


with open('./data/perf_data.pkl', 'rb') as pick:
    execution_times = pickle.load(pick)

# Plot the data
sns.set_theme()
sns.lineplot(x=n_values, y=execution_times, label="Empirical Time Complexity", )
sns.lineplot(x=n_values, y=theoretical_complexity, label="Theoretical Time Complexity (O(n**3))", linestyle='--')
plt.xlabel('n')
plt.ylabel('Average Execution Time (s)')
plt.title('Empirical vs. Theoretical Time Complexity of the Matrix Multiplication')
plt.legend()
plt.grid(True)
plt.savefig('1.png')
plt.show()
