from time import perf_counter
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pickle

matplotlib.use('TkAgg')

# Constants
num_runs = 5
n_values = list(range(1, 1001))


# Function to measure execution time for constant_function
def measure_time(n, num_runs):
    total_time = 0
    for _ in range(num_runs):
        matrix_a = np.random.randint(0, 100, (n, n))
        matrix_b = np.random.randint(0, 100, (n, n))
        start_time = perf_counter()
        np.matmul(matrix_a, matrix_b)
        end_time = perf_counter()

        total_time += end_time - start_time
        return total_time / num_runs # Average execution time


execution_times = []

# Measure execution time for each n
for n in n_values:
    avg_time = measure_time(n, num_runs)
    execution_times.append(avg_time)

with open('./data/peft_perf.pkl', 'wb') as pick:
    pickle.dump(execution_times, pick)
